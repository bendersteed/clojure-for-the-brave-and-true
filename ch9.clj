(ns clj-brave.ch9
  (:gen-class))

(future (Thread/sleep 4000)
        (println "I'll print after 4 seconds"))

(let [result (future (println "this prints once")
                     (+ 1 1))]
  (println "deref: " (deref result))
  (println "@: " @result))

;; a future's body only gets executed once and then the result is
;; cached. dereferencing it twice causes println not to be evaluated
;; twice

;; you can put a limit in how much you want to wait for a value
(deref (future (Thread/sleep 1000) 0) 10 5) ; => 5
(deref (future (Thread/sleep 1000) 0) 2000 5) ; => 0

;; realized returns t for a completed future
(realized? (future (Thread/sleep 1000)))

(def jackson-5-delay
  (delay (let [message "Just call my name and I'll be there"]
           (println "First deref:" message)
           message)))

;; promises

(def yak-butter-international
  {:store "Yak Butter International"
    :price 90
    :smoothness 90})
(def butter-than-nothing
  {:store "Butter Than Nothing"
   :price 150
   :smoothness 83})
;; This is the butter that meets our requirements
(def baby-got-yak
  {:store "Baby Got Yak"
   :price 94
   :smoothness 99})

(defn mock-api-call
  [result]
  (Thread/sleep 1000)
  result)

(defn satisfactory?
  "If the butter meets our criteria, return the butter, else return false"
  [butter]
  (and (<= (:price butter) 100)
       (>= (:smoothness butter) 97)
       butter))

(time
 (let [butter-promise (promise)]
   (doseq [butter [yak-butter-international butter-than-nothing baby-got-yak]]
     (future (if-let [satisfactory-butter (satisfactory? (mock-api-call butter))]
               (deliver butter-promise satisfactory-butter))))
   (println "Best butter is:" @butter-promise)))

;; Exercises

(def default-search-engines ["https://www.bing.com/search?q%3D"
                             "https://google.com/search?q%3D"])

;; 1 & 2
(defn search-web
  "Return the results of searching s in Google and Bing"
  ([s] (search-web s default-search-engines))
  ([s engines]
   (let [result-promise (promise)]
     (doseq [engine engines]
       (future (deliver result-promise (slurp (str engine s)))))
     @result-promise)))

;; 3
(defn get-search-results
  "Grab the raw html results of search for a s, in the search engines
  given, in a string."
  [s search-engines]
  (for [engine search-engines
        :let [result-promise (promise)]]
    (do
      (future (deliver result-promise (slurp (str engine s))))
      (flatten (conj '() @result-promise)))))


(defn get-urls
  "Get the urls in a string that resides in a list as the first element."
  [s]
  (re-seq #"https?://[^\"]*" (first s)))

(defn get-search-results-urls
  "Return all the links that appear in the search results, when
  searching for s in a various search engines in an array."
  [s search-engines]
  (vec (map get-urls (get-search-results s search-engines))))
