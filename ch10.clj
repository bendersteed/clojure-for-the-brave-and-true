(ns clj-brave.ch10
  (:gen-class))

;; cuddle zombie modeling

(def fred (atom {:cuddle-hunger-level 0
                 :percent-deteriorated 0}))

(let [zombie-state @fred]
  (if (>= (:percent-deteriorated zombie-state) 50)
    (future (println (:cuddle-hunger-level zombie-state)))))

(swap! fred
       (fn [current-state]
         (update-in current-state [:percent-deteriorated] #(+ 5 %))))

(defn increase-cuddle-hunger-level
  [zombie-state increase-by]
  (update-in zombie-state [:cuddle-hunger-level] #(+ increase-by %)))

(reset! fred {:cuddle-hunger-level 0
              :percent-deteriorated 0})

(defn shuffle-speed
  [zombie]
  (* (:cuddle-hunger-level zombie)
     (- 100 (:percent-deteriorated zombie))))

(defn shuffle-alert
  [key watched old-state new-state]
  (let [sph (shuffle-speed new-state)]
    (if (> sph 5000)
      (do
        (println "Run, you fool!")
        (println "The zombie's SPH is now " sph)
        (println "This message brought to you courtesy of " key))
      (do
        (println "All's well with " key)
        (println "SPH: " sph)))))

(add-watch fred :fred-shuffle-alert shuffle-alert)

(defn percent-deteriorated-validator
  [{:keys [percent-deteriorated]}]
  (or (and (>= percent-deteriorated 0)
           (<= percent-deteriorated 100))
      (throw (IllegalStateException. "That's not mathy!"))))

(def bobby
  (atom
   {:cuddle-hunger-level 0 :percent-deteriorated 0}
   :validator percent-deteriorated-validator))

;; sock-gnomes -- refs

(def sock-varieties
  #{"darned" "argyle" "wool" "horsehair" "mulleted"
    "passive-aggressive" "striped" "polka-dotted"
    "athletic" "business" "power" "invisible" "gollumed"})

(defn sock-count
  [sock-variety count]
  {:variety sock-variety
   :count count})

(defn generate-sock-gnome
  "Create an initial sock gnome state with no socks"
  [name]
  {:name name
   :socks #{}})

(def sock-gnome (ref (generate-sock-gnome "Barumpharumph")))
(def dryer (ref {:name "LG 1337"
                 :socks (set (map #(sock-count % 2) sock-varieties))}))

(defn steal-sock
  [gnome dryer]
  (dosync
   (when-let [pair (some #(if (= (:count %) 2) %) (:socks @dryer))]
     (let [updated-count (sock-count (:variety pair) 1)]
       (alter gnome update-in [:socks] conj updated-count)
       (alter dryer update-in [:socks] disj pair)
       (alter dryer update-in [:socks] conj updated-count)))))

;; dynamic variables

(def ^:dynamic *notification-address* "dobby@elf.org")

(binding [*notification-address* "tester-1@elf.org"]
  (println *notification-address*)
  (binding [*notification-address* "tester-2@elf.org"]
    (println *notification-address*))
  (println *notification-address*))

;; changing the var root

;; pmap

(def alphabet-length 26)

;; Vector of chars, A-Z
(def letters (mapv (comp str char (partial + 65)) (range alphabet-length)))

(defn random-string
  "Returns a random string of specified length"
  [length]
  (apply str (take length (repeatedly #(rand-nth letters)))))
  
(defn random-string-list
  [list-length string-length]
  (doall (take list-length (repeatedly (partial random-string string-length)))))

(def orc-names (random-string-list 3000 7000))

;; exercise 1
(def ex1 (atom 0))

@ex1

(swap! ex1 inc)

@ex1

;; exercise 2
(defn random-quote []
  "Returns a random quote"
  (slurp "https://www.braveclojure.com/random-quote"))

(def word-count (atom {}))

(defn count-words [s]
  (frequencies (clojure.string/split (.toUpperCase s) #"\W+")))

(defn update-word-count []
  (swap! word-count (fn [prev] (merge-with + (count-words (random-quote))))))

(defn quote-word-count [n]
  (let [promises (take n (repeatedly promise))]
    (doseq [p promises]
      (future
        (update-word-count)
        (deliver p true)))
    (every? deref promises))
  @word-count)

(quote-word-count 5)

;;exercise 3
(def harold-the-wounded (ref {:hit-points 15, :items []}))
(def arnold-the-vain (ref {:hit-points 40, :items ["h-potion"]}))

(defn give-potion [giv rec]
  (dosync
   (alter giv update-in [:items] remove "h-potion")
   (alter rec update-in [:hit-points] + 15)))

(give-potion arnold-the-vain harold-the-wounded)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; could be better, items as a map with counts decrement or remove altogether ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
