(ns clj-brave.ch8
  (:gen-class))

;; when is defined using macro and if, do
(macroexpand '(when boolen-expr
                expr1
                expr2
                expr3)) ;=> (if boolen-expr (do expr1 expr2 expr3))


;; syntax quoting
`(+ 1 ~(inc 2)) ;=> (clojure.core/+ 1 3)

;; code critic example
(defmacro code-critic-q
  "Phrases are courtesy Hermes Conrad from Futurama"
  [bad good]
  (list 'do
        (list 'println
              "Great squid of Madrid, this is bad code:"
              (list 'quote bad))
        (list 'println
              "Sweet gorilla of Manila, this is good code:"
              (list 'quote good))))


                                        ; it can be written more consisely with syntax-quoting
(defmacro code-critic-s
  "Phrases are courtesy Hermes Conrad from Futurama"
  [bad good]
  `(do (println "Great squid of Madrid, this is bad code:"
                (quote ~bad))
       (println "Sweet gorilla of Manila, this is good code:"
                (quote ~good))))

                                        ; even better with some helper functions
(defn criticize-code
  [criticism code]
  `(println ~criticism (quote ~code)))

(defmacro code-critic-h
  [bad good]
  `(do ~(criticize-code "Great squid of Madrid, this is bad code:" bad)
       ~(criticize-code "Sweet gorilla of Manila, this is good code:" good)))

                                        ; let's use map with unquote splicing
(defmacro code-critic
  [bad good]
  `(do ~@(map #(apply criticize-code %)
              [["Sweet gorilla of Manila, this is good code:" bad]
               ["Great cow of Moscow, this is good code:" good]])))

;; things to watch out - variables in macros
(def msg "Good job")
(defmacro with-mischief
  [& stuff-to-do]
  (concat (list 'let ['msg "Oh, big deal!"])
          stuff-to-do))

(defmacro without-mischief
  [& stuff-to-do]
  (let [macro-msg (gensym 'msg)]
    `(let [~macro-msg "Oh, big deal!"]
       ~@stuff-to-do
       (println "I still need to say: " ~macro-msg))))

;; double evaluation
(defmacro report
  [to-try]
  `(if ~to-try
     (println (quote ~to-try) "was succesful: " ~to-try)
     (println (quote ~to-try) "was unsuccesful: " ~to-try)))

(report (do (Thread/sleep 1000) (+ 1 1))) ;to-try gets evaluated twice per branch

(defmacro better-report
  [to-try]
  `(let [result# ~to-try]
     (if result#
       (println (quote ~to-try) "was succesful: " result#)
       (println (quote ~to-try) "was unsuccesful: " result#))))

(better-report (do (Thread/sleep 1000) (+ 1 1))) ;now it's better

;; brews for the brave and true
(def order-details
  {:name "Twisted Bald"
   :email "her@hey.gr"})

(def order-detail-validations
  {:name
   ["Please enter a name" not-empty]

   :email
   ["Please enter an email address" not-empty

    "Your email address doesn't seem like an email address"
    #(or (empty? %) (re-seq #"@" %))]})

(defn error-msg-for
  "Return a seq of error messages"
  [to-validate message-validator-pairs]
  (map first (filter #(not ((second %) to-validate))
                     (partition 2 message-validator-pairs))))

(defn validate
  "Return a map with a vector of errors for each key"
  [to-validate validations]
  (reduce (fn [errors validation]
            (let [[fieldname validation-check-groups] validation
                  value (get to-validate fieldname)
                  error-messages (error-msg-for value validation-check-groups)]
              (if (empty? error-messages)
                errors
                (assoc errors fieldname error-messages))))
          {}
          validations))

(defmacro if-valid
  "Handle validation more concisely"
  [to-validate validations errors-name & then-else]
  `(let [~errors-name (validate ~to-validate ~validations)]
     (if (empty? ~errors-name)
       ~@then-else)))

(macroexpand '(if-valid order-details order-detail-validations errors
           (str :success)
           (str :failure errors)))

;;(let* [errors (clj-brave.ch8/validate order-details order-detail-validations)]
;;(if (clojure.core/empty? errors) (str :success) (str :failure errors)))
;; we expose error so we can access it externally

;; Exercise 1
(defmacro when-valid
  "Use validations to execute when conditinal"
  [to-validate validations & body]
  `(let [errors# (validate ~to-validate ~validations)]
     (when (empty? errors#)
       ~@body)))

(macroexpand '(when-valid order-details order-detail-validations (println "it's a success")))

;; (let* [errors__7387__auto__ (clj-brave.ch8/validate order-details
;; order-detail-validations)] (clojure.core/when (clojure.core/empty?
;; errors__7387__auto__) (println "it's a success")))

;; Exercise 2
(defmacro my-or
  "Evaluate each expression and return the first true. If all non-true
  return nil."
  ([] nil)
  ([x] x)
  ([x & next]
   `(let [or# ~x]
      (if or# or# (my-or ~@next)))))
;; Exercise 3
(defmacro defattrs
  "Define functions for accessing character attributes like in chapter
  5."
  ([] nil)
  ([fn attr]
   `(def ~fn (comp ~attr :attributes)))
  ([fn attr & rest]
   `(do
      (defattrs ~fn ~attr)
      (defattrs ~@rest))))

(macroexpand '(defattrs c-str :strength
                c-int :intelligence))
