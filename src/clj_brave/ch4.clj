(ns clj-brave.ch4
  (:gen-class))
(use 'clojure.pprint)
;; Chapter 4 - Examples on seq functions

(def human-consumption [8.1 7.3 6.6 5.0])
(def critter-consumption [0.0 0.2 0.3 1.1])
(defn unify-diet-data
  [human critter]
  {:human human :critter critter})

(defn my-map
  [f lst]
  (if (empty? lst)
    '()
    (cons (f (first lst)) (map f (rest lst)))))

;;; map examples
;; map can be used to pass a collection of functions
;; here we apply sum count and avg on a list
;; and we get a list of the computed values
(def sum #(reduce + %))
(def avg #(/ (sum %) (count %)))
(defn stats
  [numbers]
  (map #(% numbers) [sum count avg]))

(stats '(10 10 10))

;; we can also retrieve values from map data
(def identities
  [{:alias "Batman" :real "Bruce Wayne"}
   {:alias "Spider-Man" :real "Peter Parker"}
   {:alias "Santa" :real "Your mom"}
   {:alias "Easter Bunny" :real "Your dad"}])

(map :alias identities)

;;; reduce examples
;; returning a new map with updates valuses
(reduce (fn [new-map [key val]]
          (assoc new-map key (inc val)))
        {}
        {:max 30 :min 8})

;; filtering map based on values
(reduce (fn [new-map [key val]]
          (if (> val 4)
            (assoc new-map key val)
            new-map))
        {}
        {:human 4.2 :critter 3.9 :various 5.0})

(defn my-map-reduce
  "Implementing map with reduce"
  [f lst]
  (reduce (fn [new-seq lst]
            (cons (f lst) new-seq))
          '()
          (reverse lst)))

;;; take, drop, take-while, drop-while, filter and some
(def food-journal
  [{:month 1 :day 1 :human 5.3 :critter 2.3}
   {:month 1 :day 2 :human 5.1 :critter 2.0}
   {:month 2 :day 1 :human 4.9 :critter 2.1}
   {:month 2 :day 2 :human 5.0 :critter 2.5}
   {:month 3 :day 1 :human 4.2 :critter 3.3}
   {:month 3 :day 2 :human 4.0 :critter 3.8}
   {:month 4 :day 1 :human 3.7 :critter 3.9}
   {:month 4 :day 2 :human 3.7 :critter 3.6}])

(take-while #(< (:month %) 4) (drop-while #(< (:month %) 2) food-journal))

(filter #(< (:human %) 5) food-journal)

(some #(> (:critter %) 5) food-journal)

(some #(and (> (:critter %) 3) %) food-journal)

;;; sort, sort-by
(sort-by count ['(1 2 3) '(3 4 5 6) '(6 7) '(8) '(7 8 9)])


;; Chapter 4 - Vampire Hunting - Lazy seq

(def vampire-database
  {0 {:makes-blood-puns? false, :has-pulse? true  :name "McFishwich"}
   1 {:makes-blood-puns? false, :has-pulse? true  :name "McMackson"}
   2 {:makes-blood-puns? true,  :has-pulse? false :name "Damon Salvatore"}
   3 {:makes-blood-puns? true,  :has-pulse? true  :name "Mickey Mouse"}})

(defn vampire-related-details
  [social-security-number]
  (Thread/sleep 1000)
  (get vampire-database social-security-number))

(defn vampire?
  [record]
  (and (:makes-blood-puns? record)
       (not (:has-pulse? record))
       record))

(defn identify-vampire
  [social-security-numbers]
  (filter vampire?
          (map vampire-related-details social-security-numbers)))

;; infinite seqs
(defn even-numbers
  ([] (even-numbers 0))
  ([n] (cons n (lazy-seq (even-numbers (+ n 2))))))

(take 10 (even-numbers))

;; collection functions

;; into can be used to return a seq to the former data structure

(into {} (map identity {:sunlight-reaction "Glitter!"}))

;; conj adds the whole second argument to the collection
;; it can be used to add elements!

(conj [0] [1])
(conj [0] 1 2 3 4)

(defn my-conj
  [target & additions]
  (into target additions))

;; Vampire Data Analysis Program

(def filename "suspects.csv")

(slurp filename)

(def vamp-keys [:name :glitter-index])

(defn str->int
  [str]
  (Integer. str))

(def conversions {:name identity
                  :glitter-index str->int})

(defn convert
  [vamp-key value]
  ((get conversions vamp-key) value))


(defn parse
  "Convert a CSV into rows of columns"
  [string]
  (map #(clojure.string/split % #",")
       (clojure.string/split string #"\n")))

(parse (slurp filename))

(defn mapify
  "Return a seq of maps"
  [rows]
  (map (fn [unmappeed-row]
         (reduce (fn [row-map [vamp-key value]]
                   (assoc row-map vamp-key
                          (convert vamp-key value)))
                 {}
                 (map vector vamp-keys unmappeed-row)))
       rows))

(mapify (parse (slurp filename)))

(defn glitter-filter
  [minimum-glitter records]
  (filter #(>= (:glitter-index %) minimum-glitter)
          records))

(glitter-filter 3 (mapify (parse (slurp filename))))

;; Exercise 1
(defn glitter-filter-names
  "Return a list of names given a glitter limit"
  [minimum-glitter records]
  (map :name
       (glitter-filter minimum-glitter records)))

(glitter-filter-names 3 (mapify (parse (slurp filename))))

;; Exercise 2
(defn append
  "Add a suspect to the map of suspects"
  [suspect suspects]
  (conj suspects suspect))

;;Exercise 3
(def is-valid? (complement nil?))

(def validators {:name is-valid?
                 :glitter-index is-valid?})

(defn validate
  "Check if a record contains :name and :glitter-index"
  [validators record]
  (reduce (fn [val vamp-keys]
            (and val
             (get record vamp-keys)
             ((get validators vamp-keys) record)))
          true
          vamp-keys))

;; Exercise 4
(defn write-vamps-to-csv
  "Creates a csv string from list of maps"
  [records]
  (clojure.string/join "\n"
                       (map (fn [record]
                              (clojure.string/join
                               "," (list (:name record)
                                         (:glitter-index record))))
                            records)))


;; main
(defn -main
  [& args]
  (clojure.pprint/pprint (mapify (parse (slurp filename)))))
