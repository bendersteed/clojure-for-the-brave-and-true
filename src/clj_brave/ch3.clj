(ns clj-brave.ch3
  (:gen-class))
(use 'clojure.pprint)

;; Chapter 3 - Hobbit smacker

(def asym-hobbit-body-parts [{:name "head" :size 3}
                             {:name "left-eye" :size 1}
                             {:name "left-ear" :size 1}
                             {:name "mouth" :size 1}
                             {:name "nose" :size 1}
                             {:name "neck" :size 2}
                             {:name "left-shoulder" :size 3}
                             {:name "left-upper-arm" :size 3}
                             {:name "chest" :size 10}
                             {:name "back" :size 10}
                             {:name "left-forearm" :size 3}
                             {:name "abdomen" :size 6}
                             {:name "left-kidney" :size 1}
                             {:name "left-hand" :size 2}
                             {:name "left-knee" :size 2}
                             {:name "left-thigh" :size 4}
                             {:name "left-lower-leg" :size 3}
                             {:name "left-achilles" :size 1}
                             {:name "left-foot" :size 2}])


(defn matching-part
  "Take a hobbit part and return the symmetrical part"
  [part]
  {:name (clojure.string/replace (:name part) #"^left" "right")
   :size (:size part)})

(defn symmetrize-body-parts
  "Expect a left aligned anatomy seq of maps using name and size keywords,
  return the full anatomy"
  [asym-body-parts]
  (loop [remaining-asym-parts asym-body-parts
         final-body-parts []]
    (if (empty? remaining-asym-parts)
      final-body-parts
      (let [[part & remaining] remaining-asym-parts]
        (recur remaining
               (into final-body-parts
                     (set [part (matching-part part)])))))))


(defn better-symmetrize-body-parts
  "As symmetrize body parts but using reduce"
  [asym-body-parts]
  (reduce (fn [final-body-parts part]
            (into final-body-parts
                  (set [part (matching-part part)])))
          []
          asym-body-parts))

(defn hit
  "Hit a random part of an asym body-parts collection"
  [asym-body-parts]
  (let [sym-parts (better-symmetrize-body-parts asym-body-parts)
        body-part-size-sum (reduce + (map :size sym-parts))
        target (rand body-part-size-sum)]
    (loop [[part & remaining] sym-parts
           accumulated-size (:size part)]
      (if (> accumulated-size target)
        part
        (recur remaining (+ accumulated-size (:size (first remaining))))))))

;;; Chapter 3 - Exercises

;; Exercise 1
(str "Hey " "Jude" ", " "don't " "be " "afraid")
(vector "a" "b" "c")
(list "hey" "hey" "hey")
(hash-map :first 1 :second 2 :third 3)
(hash-set :a 1 :a 1 :b 2 "hey")

;;Exercise 2
(defn add100
  [n]
  (+ n 100))

;;Exercise 3
(defn dec-maker
  "Return a custom decrementer by given value"
  [dec-by]
  #(- % dec-by))

(def d5 (dec-maker 5))

;;Exercise 4
(defn mapset
  "Works like map but returns a set instead of a seq"
  [f seq]
  (set (map f seq)))

;;Exercise 5
(def asym-alien-body-parts [{:name "head" :size 3}
                             {:name "first-eye" :size 1}
                             {:name "first-ear" :size 1}
                             {:name "mouth" :size 1}
                             {:name "nose" :size 1}
                             {:name "neck" :size 2}
                             {:name "first-shoulder" :size 3}
                             {:name "first-upper-arm" :size 3}
                             {:name "chest" :size 10}
                             {:name "back" :size 10}
                             {:name "first-forearm" :size 3}
                             {:name "abdomen" :size 6}
                             {:name "first-kidney" :size 1}
                             {:name "first-hand" :size 2}
                             {:name "first-knee" :size 2}
                             {:name "first-thigh" :size 4}
                             {:name "first-lower-leg" :size 3}
                             {:name "first-achilles" :size 1}
                             {:name "first-foot" :size 2}])

(defn alien-matching-part
  "Returns one of the possible matching parts given the val of pos"
  [part pos]
  {:name (clojure.string/replace (:name part) #"^first" (str pos))
   :size (:size part)})

(defn alien-matching-parts
  "Returns all the matching parts"
  [part]
  (if (re-find #"^first" (:name part))
   (loop [seq (list "second" "third" "fourth" "fifth")
          matching-parts [part]]
     (if (empty? seq)
       matching-parts
       (let [[first & rest] seq]
         (recur rest
                (into matching-parts
                      [(alien-matching-part part first)])))))
   [part]))

(defn alien-symmetrize-body-parts
  "For aliens with five of each body bart"
  [asym-body-parts]
  (reduce (fn [final-body-parts part]
            (into final-body-parts
                  (alien-matching-parts part)))
          []
          asym-body-parts))

;;Exercise 6
(defn build-a-count-list
  "Given 5 returns (2 3 4 5)"
  [n]
  (loop [i 2
         lst '()]
    (if (<= i n)
      (recur (inc i) (into lst (list i)))
      (sort lst))))

(def asym-body-parts [{:name "head" :size 3}
                      {:name "1-eye" :size 1}
                      {:name "1-ear" :size 1}
                      {:name "mouth" :size 1}
                      {:name "nose" :size 1}
                      {:name "neck" :size 2}
                      {:name "1-shoulder" :size 3}
                      {:name "1-upper-arm" :size 3}
                      {:name "chest" :size 10}
                      {:name "back" :size 10}
                      {:name "1-forearm" :size 3}
                      {:name "abdomen" :size 6}
                      {:name "1-kidney" :size 1}
                      {:name "1-hand" :size 2}
                      {:name "1-knee" :size 2}
                      {:name "1-thigh" :size 4}
                      {:name "1-lower-leg" :size 3}
                      {:name "1-achilles" :size 1}
                      {:name "1-foot" :size 2}])

(defn general-matching-part
  "Returns one of the possible matching parts given the val of pos"
  [part pos]
  {:name (clojure.string/replace (:name part) #"^1" (str pos))
   :size (:size part)})

(defn general-matching-parts
  "Returns all the matching parts, given a part and the number that the species has"
  [part n]
  (if (re-find #"^1" (:name part))
   (loop [seq (build-a-count-list n)
          matching-parts [part]]
     (if (empty? seq)
       matching-parts
       (let [[first & rest] seq]
         (recur rest
                (into matching-parts
                      [(general-matching-part part first)])))))
   [part]))

(defn general-symmetrize-body-parts
  "Yay!"
  [asym-body-parts n]
  (reduce (fn [final-body-parts part]
            (into final-body-parts
                  (general-matching-parts part n)))
          []
          asym-body-parts))

(defn -main
  [& args]
  (clojure.pprint/pprint (general-symmetrize-body-parts asym-body-parts 5)))
