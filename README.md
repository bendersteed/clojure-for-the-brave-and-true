## clj-brave

Some solutions for the very interesting book ["Clojure for the Brave
and True"](https://www.braveclojure.com/ "Check out the book and maybe
buy it if you like it!"). They are organised using
[leiningen](https://leiningen.org/ "Get it here!") so you may want to
have it installed if you want to check them out!

## Installation & Usage

Just clone the repo and run:

``` shell
	lein run
```
from the repos root. This should show you some results from the latest
exercise I've written. I will organize the project later so there will
be access to all Chapters' exercises from lein.

For now you can change the target line to the chapter you want to run.

## Changelog

- Completed Chapter 10 exercises, January 17, 2019
- Completed Chapter 9, exercises, January 3, 2019
- Completed Chapter 7,8, both notes and exercises. December 23, 2018
- Completed Chapter 6 notes. November 25, 2018
- Completed Chapter 5 exercises. November 25, 2018
- Add notes for Chapter 5, run lein run to play PegThing. November 24, 2018
- Completed Chapter 4 exercises. November 24, 2018
- Completed Chapter 3 exercises. November 21, 2018

## License

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
